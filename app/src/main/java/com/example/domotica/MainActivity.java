package com.example.domotica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnEnviar;
    TextView Usuario;
    TextView Password;
    String user = "admin";
    String password = "admin123";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Usuario = findViewById(R.id.txtUsuario);
        Password = findViewById(R.id.txtPassword);

        btnEnviar = findViewById(R.id.btnEnviar);
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user.contentEquals(Usuario.getText())){
                    if(password.contentEquals(Password.getText())){
                        Intent myIntent = new Intent(MainActivity.this, Menu.class);
                        startActivity(myIntent);
                    }else{
                        Toast.makeText(MainActivity.this, "contraseña Incorrectos", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(MainActivity.this, "Usuario Incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
