package com.example.domotica;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class PlantaAlta extends AppCompatActivity {

    SwitchMaterial Room1;
    SwitchMaterial Room2;
    SwitchMaterial Room3;
    SwitchMaterial Bathroom;
    SwitchMaterial Escaleras;

    String statusRoom1;
    String statusRoom2;
    String statusRoom3;
    String statusBathroom;
    String statusEscaleras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planta_alta);
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        Room1 = findViewById(R.id.room1);
        Room2 = findViewById(R.id.room2);
        Room3 = findViewById(R.id.room3);
        Bathroom = findViewById(R.id.bathroom);
        Escaleras = findViewById(R.id.escaleras);
    }

    public void onClickRoom1(View view) {
        if (view.getId()==R.id.room1){
            if (Room1.isChecked()){
                statusRoom1 = "ON";
                Toast.makeText(this, "Luz Room 1: "+statusRoom1, Toast.LENGTH_SHORT).show();

            }else{
                statusRoom1 = "OFF";
                Toast.makeText(this, "Luz Room 1: "+statusRoom1, Toast.LENGTH_SHORT).show();

            }
        }
    }

    public void onClickRoom2(View view) {
        if (view.getId()==R.id.room2){
            if (Room2.isChecked()){
                statusRoom2 = "ON";
                Toast.makeText(this, "Luz Room 2: "+statusRoom2, Toast.LENGTH_SHORT).show();
            }else{
                statusRoom2 = "OFF";
                Toast.makeText(this, "Luz Room 2: "+statusRoom2, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onClickRoom3(View view) {
        if (view.getId()==R.id.room3){
            if (Room3.isChecked()){
                statusRoom3 = "ON";
                Toast.makeText(this, "Luz Room 3: "+statusRoom3, Toast.LENGTH_SHORT).show();
            }else{
                statusRoom3 = "OFF";
                Toast.makeText(this, "Luz Room 3: "+statusRoom3, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onClickBathroom(View view) {
        if (view.getId()==R.id.bathroom){
            if (Bathroom.isChecked()){
                statusBathroom = "ON";
                Toast.makeText(this, "Luz Baño: "+statusBathroom, Toast.LENGTH_SHORT).show();
            }else{
                statusBathroom = "OFF";
                Toast.makeText(this, "Luz Baño: "+statusBathroom, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onClickEscaleras(View view) {
        if (view.getId()==R.id.escaleras){
            if (Escaleras.isChecked()){
                statusEscaleras = "ON";
                Toast.makeText(this, "Luz Escaleras: "+statusEscaleras, Toast.LENGTH_SHORT).show();
            }else{
                statusEscaleras = "OFF";
                Toast.makeText(this, "Luz Escaleras: "+statusEscaleras, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
