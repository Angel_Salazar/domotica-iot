package com.example.domotica;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Patio extends AppCompatActivity {

    SwitchMaterial Illuminacion;
    SwitchMaterial AspersoresTraceros;
    SwitchMaterial AspersoresDelanteros;
    TextView txtHumTra;
    TextView txtHumDel;

    String statusIlluminacion;
    String statusAspTra;
    String statusAspDel;
    String txtHumeTra = "50";
    String txtHumeDel = "50";
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patio);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        Illuminacion = findViewById(R.id.illuminacion);
        txtHumTra = findViewById(R.id.txtHumedadTracera);
        txtHumTra.setText(txtHumeTra+"%");
        txtHumDel = findViewById(R.id.txtHumedadDelantera);
        txtHumDel.setText(txtHumeDel+"%");
        AspersoresTraceros = findViewById(R.id.aspersoresTraceros);
        AspersoresDelanteros = findViewById(R.id.aspersoresDelanteros);
    }

    public void onClickIllumanacion(View view) {
        if (view.getId()==R.id.illuminacion){
            if (Illuminacion.isChecked()){
                statusIlluminacion = "ON";
                String mensaje = statusIlluminacion;
                mDatabase.child("LIGHT_STATUS").setValue(mensaje);
                Toast.makeText(this, "Illuminacion: "+statusIlluminacion, Toast.LENGTH_SHORT).show();
            }else{
                statusIlluminacion = "OFF";
                Toast.makeText(this, "Illuminacion: "+statusIlluminacion, Toast.LENGTH_SHORT).show();
                String mensaje = statusIlluminacion;
                mDatabase.child("LIGHT_STATUS").setValue(mensaje);
            }
        }
    }

    public void onClickAspTra(View view) {
        if (view.getId()==R.id.aspersoresTraceros){
            if (AspersoresTraceros.isChecked()){
                statusAspTra = "ON";
                Toast.makeText(this, "Aspersores Traceros: "+statusAspTra, Toast.LENGTH_SHORT).show();
            }else{
                statusAspTra = "OFF";
                Toast.makeText(this, "Aspersores Traceros: "+statusAspTra, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onClickAspDel(View view) {
        if (view.getId()==R.id.aspersoresDelanteros){
            if (AspersoresDelanteros.isChecked()){
                statusAspDel = "ON";
                Toast.makeText(this, "Aspersores Delanteros: "+statusAspDel, Toast.LENGTH_SHORT).show();
            }else{
                statusAspDel = "OFF";
                Toast.makeText(this, "Aspersores Delanteros: "+statusAspDel, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
