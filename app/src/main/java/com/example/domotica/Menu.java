package com.example.domotica;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Menu extends AppCompatActivity {

    CardView PlantaAlta;
    CardView PlantaBaja;
    CardView Patio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        PlantaAlta = findViewById(R.id.plantaAlta);
        PlantaAlta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Menu.this, com.example.domotica.PlantaAlta.class);
                startActivity(myIntent);
            }
        });

        PlantaBaja = findViewById(R.id.plantaBaja);
        PlantaBaja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Menu.this, com.example.domotica.PlantaBaja.class);
                startActivity(myIntent);
            }
        });

        Patio = findViewById(R.id.patio);
        Patio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Menu.this, Patio.class);
                startActivity(myIntent);
            }
        });
    }
}
