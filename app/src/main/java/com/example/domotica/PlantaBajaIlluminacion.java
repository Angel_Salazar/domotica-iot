package com.example.domotica;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class PlantaBajaIlluminacion extends AppCompatActivity {

    SwitchMaterial CuartoLavado;
    SwitchMaterial Cocina;
    SwitchMaterial Sala;
    SwitchMaterial Comedor;
    SwitchMaterial Cuarto;

    String statusCuartoLavado;
    String statusCocina;
    String statusSala;
    String statusComedor;
    String statusCuarto;
    private DatabaseReference mDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planta_baja_illuminacion);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        CuartoLavado = findViewById(R.id.cuartoLavado);
        Cocina = findViewById(R.id.cocina);
        Sala = findViewById(R.id.sala);
        Comedor = findViewById(R.id.comedor);
        Cuarto = findViewById(R.id.cuarto);
    }

    public void onClickRoom1(View view) {
        if (view.getId()==R.id.cuartoLavado){
            if (CuartoLavado.isChecked()){
                statusCuartoLavado = "ON";
                Toast.makeText(this, "Luz Cuarto de Lavado: "+statusCuartoLavado, Toast.LENGTH_SHORT).show();
                String mensaje = statusCuartoLavado;
                mDatabase.child("LIGHT_LAVADO_STATUS").setValue(mensaje);
            }else{
                statusCuartoLavado = "OFF";
                Toast.makeText(this, "Luz Cuarto de Lavado: "+statusCuartoLavado, Toast.LENGTH_SHORT).show();
                String mensaje = statusCuartoLavado;
                mDatabase.child("LIGHT_LAVADO_STATUS").setValue(mensaje);
            }
        }
    }

    public void onClickRoom2(View view) {
        if (view.getId()==R.id.cocina){
            if (Cocina.isChecked()){
                statusCocina = "ON";
                Toast.makeText(this, "Luz Cocina: "+statusCocina, Toast.LENGTH_SHORT).show();
                String mensaje = statusCocina;
                mDatabase.child("LIGHT_COCINA_STATUS").setValue(mensaje);
            }else{
                statusCocina = "OFF";
                Toast.makeText(this, "Luz Cocina: "+statusCocina, Toast.LENGTH_SHORT).show();
                String mensaje = statusCocina;
                mDatabase.child("LIGHT_COCINA_STATUS").setValue(mensaje);
            }
        }
    }

    public void onClickRoom3(View view) {
        if (view.getId()==R.id.sala){
            if (Sala.isChecked()){
                statusSala = "ON";
                Toast.makeText(this, "Luz Sala: "+statusSala, Toast.LENGTH_SHORT).show();
                String mensaje = statusSala;
                mDatabase.child("LIGHT_SALA_STATUS").setValue(mensaje);
            }else{
                statusSala = "OFF";
                Toast.makeText(this, "Luz Sala: "+statusSala, Toast.LENGTH_SHORT).show();
                String mensaje = statusSala;
                mDatabase.child("LIGHT_SALA_STATUS").setValue(mensaje);
            }
        }
    }

    public void onClickBathroom(View view) {
        if (view.getId()==R.id.comedor){
            if (Comedor.isChecked()){
                statusComedor = "ON";
                Toast.makeText(this, "Luz Comedor: "+statusComedor, Toast.LENGTH_SHORT).show();
                String mensaje = statusComedor;
                mDatabase.child("LIGHT_COMEDOR_STATUS").setValue(mensaje);
            }else{
                statusComedor = "OFF";
                Toast.makeText(this, "Luz Comedor: "+statusComedor, Toast.LENGTH_SHORT).show();
                String mensaje = statusComedor;
                mDatabase.child("LIGHT_COMEDOR_STATUS").setValue(mensaje);
            }
        }
    }

    public void onClickEscaleras(View view) {
        if (view.getId()==R.id.cuarto){
            if (Cuarto.isChecked()){
                statusCuarto = "ON";
                Toast.makeText(this, "Luz Cuarto: "+statusCuarto, Toast.LENGTH_SHORT).show();
                String mensaje = statusCuarto;
                mDatabase.child("LIGHT_CUARTO_STATUS").setValue(mensaje);
            }else{
                statusCuarto = "OFF";
                Toast.makeText(this, "Luz Cuarto: "+statusCuarto, Toast.LENGTH_SHORT).show();
                String mensaje = statusCuarto;
                mDatabase.child("LIGHT_CUARTO_STATUS").setValue(mensaje);
            }
        }
    }
}
