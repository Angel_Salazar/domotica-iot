package com.example.domotica;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class PlantaBaja extends AppCompatActivity {

    CardView Illuminacion;
    SwitchMaterial Camaras;
    SwitchMaterial Puerta;
    SwitchMaterial Ventilador;
    Button boton1;

    String statusVentilador;
    String statusPuerta;
    String statusCamaras;

    private DatabaseReference mDatabase;
    private EditText edit1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planta_baja);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Illuminacion = findViewById(R.id.plantaBaja);
        Illuminacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(PlantaBaja.this, PlantaBajaIlluminacion.class);
                startActivity(myIntent);
            }
        });

        Camaras = findViewById(R.id.camaras);
        Puerta = findViewById(R.id.puerta);
        Ventilador = findViewById(R.id.ventilador);
        //boton1 =  findViewById(R.id.btn1);
        //edit1 = (EditText) findViewById(R.id.editText1);
/*
        boton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mensaje = edit1.getText().toString();
                mDatabase.child("status").setValue(mensaje);
            }
        });*/
    }

    public void onClickCamaras(View view) {
        if (view.getId() == R.id.camaras) {
            if (Camaras.isChecked()) {
                statusCamaras = "ON";
                Toast.makeText(this, "Camaras: " + statusCamaras, Toast.LENGTH_SHORT).show();
                String mensaje = statusCamaras;
                mDatabase.child("CAM_STATUS").setValue(mensaje);
            } else {
                statusCamaras = "OFF";
                Toast.makeText(this, "Camaras: " + statusCamaras, Toast.LENGTH_SHORT).show();
                String mensaje = statusCamaras;
                mDatabase.child("CAM_STATUS").setValue(mensaje);
            }
        }
    }

    public void onClickPuerta(View view) {
        if (view.getId() == R.id.puerta) {
            if (Puerta.isChecked()) {
                statusPuerta = "ON";
                Toast.makeText(this, "Puerta :" + statusPuerta, Toast.LENGTH_SHORT).show();
                String mensaje = statusPuerta;
                mDatabase.child("DOOR_STATUS").setValue(mensaje);
            } else {
                statusPuerta = "OFF";
                Toast.makeText(this, "Puerta :" + statusPuerta, Toast.LENGTH_SHORT).show();
                String mensaje = statusPuerta;
                mDatabase.child("DOOR_STATUS").setValue(mensaje);
            }
        }
    }

    public void onClickVentilador(View view) {
        if (view.getId() == R.id.ventilador) {
            if (Ventilador.isChecked()) {
                statusVentilador = "ON";
                Toast.makeText(this, "Ventilador: " + statusVentilador, Toast.LENGTH_SHORT).show();
                String mensaje = statusVentilador;
                mDatabase.child("VENT_STATUS").setValue(mensaje);
            } else {
                statusVentilador = "OFF";
                Toast.makeText(this, "Ventilador: " + statusVentilador, Toast.LENGTH_SHORT).show();
                String mensaje = statusVentilador;
                mDatabase.child("VENT_STATUS").setValue(mensaje);
            }
        }
    }

}